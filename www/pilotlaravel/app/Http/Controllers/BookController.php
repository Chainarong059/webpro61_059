<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookController extends Controller
{
    //
    /*function store(Request $request){
        $book = new Book();
        $book->setAttribute("name",$request->name);
        $book->SetAttribute("category",$request->category);
        $book->SetAttribute("description",$request->descrition);
        if ($book->save()){
            return true;
        }
       }*/
    function store(Request $request){
        if (Book::created($request->all())){
            return true;
        }
    }
    function update(Request $request){
        if (Book::fill($request->all)->save()){
            return true;
        }
    }
    function index(){
        $books = Book::All();
        return $books;
    }
    function show(Book $book){
        return $book;
    }
    function destroy(Book $book){
        if ($book->delete()){
            return true;
        }
    }
}
